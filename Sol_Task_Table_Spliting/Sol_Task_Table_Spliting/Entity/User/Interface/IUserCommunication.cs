﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Table_Spliting.Entity.User.Interface
{
    interface IUserCommunication
    {
        int UserId { get; set; }

        String MobileNo { get; set; }

        String EmailId { get; set; }
    }
}
