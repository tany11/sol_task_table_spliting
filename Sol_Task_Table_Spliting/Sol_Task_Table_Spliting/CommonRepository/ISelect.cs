﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Task_Table_Spliting.CommonRepository
{
    public interface ISelect<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> Select(TEntity entityObj);
    }
}
