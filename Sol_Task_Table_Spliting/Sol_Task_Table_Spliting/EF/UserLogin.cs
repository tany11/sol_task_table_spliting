//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sol_Task_Table_Spliting.EF
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserLogin
    {
        public decimal LoginId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public decimal tblUserUserId { get; set; }
    
        public virtual tblUser tblUser { get; set; }
    }
}
